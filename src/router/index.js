import Vue from 'vue'
import VueRouter from 'vue-router'

// Containers
const TheContainer = () => import('@/containers/TheContainer')

// Views - Components
const Home = () => import('@/views/Home')
const PostDetail = () => import('@/views/PostDetail')
const CreatePost = () => import('@/views/CreatePost')

Vue.use(VueRouter)

/*const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',    
    component: () => import('../views/About.vue')
  }
]*/

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,  
  linkActiveClass: 'active',
  scrollBehavior: () => ({ y: 0 }),
  routes: configRoutes()
})

function configRoutes () {
  return [
    {
      path: '/',
      redirect: '/home',
      name: 'Home',
      component: TheContainer,
      children: [
        {
          path: 'home',
          name: 'Posts',
          component: Home
        },
        {
          path: 'posts/:id',
          name: 'PostDetail',
          component: PostDetail
        },
        {
          path: 'posts',
          name: 'CreatePost',
          component: CreatePost
        },
      ]
    }
  ]
}

export default router
