export default [
  {
    _name: 'CSidebarNav',
    _children: [                    
      {
        _name: 'CSidebarNavItem',
        name: 'Home',
        to: '/home',        
      },  
      {
        _name: 'CSidebarNavItem',
        name: 'Create Post',
        to: '/posts',        
      },                           
    ]
  }
]