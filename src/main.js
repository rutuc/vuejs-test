import Vue from 'vue'
import App from './App.vue'
import axios from 'axios'
import router from './router'
import store from './store'
import CoreuiVue from '@coreui/vue'
import Vuelidate from 'vuelidate'

Vue.config.productionTip = false
Vue.use(CoreuiVue)
Vue.use(Vuelidate)

Vue.prototype.$axios = axios;
axios.defaults.baseURL = "https://jsonplaceholder.typicode.com";

new Vue({
  el: '#app',
  router,
  store,  
  template: '<App/>',
  components: {
    App
  }
})